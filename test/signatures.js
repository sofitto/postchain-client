var secp256k1 = require('secp256k1');
var util = require('../src/util');

var sig = (num) => {
    var privKey = Buffer.from("1234567890123456789012345678901" + num);
    var pubKey = secp256k1.publicKeyCreate(privKey);
    var signature = util.sign(Buffer.from('hello', 'utf8'), privKey);
    return {pubKey: pubKey, signature: signature, privKey: privKey};
}

module.exports.signatures = (count) => {
    var result = [];
    for (var i = 0; i < count; i++) {
        result.push(sig(i).signature);
    }
    return result;
}

module.exports.signers = (count) => {
    var result = [];
    for (var i = 0; i < count; i++) {
        result.push(sig(i).pubKey);
    }
    return result;
}

module.exports.privKey = (index) => {
    return sig(index).privKey;
}