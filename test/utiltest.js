var util = require('../src/util');
var assert = require('assert');
describe('Util', function () {
    function merkleRoot(stringList) {
        return util.calculateMerkleRoot(hashList(stringList));
    }

    function stringToHash(string) {
        return util.hash256(Buffer.from(string));
    }

    function hashList(stringList) {
        return stringList.map(stringToHash);
    }

    function checkDifferent(list1, list2) {
        var root1 = merkleRoot(list1);
        var root2 = merkleRoot(list2);
        assert.ok(!root1.equals(root2));
    }
    var a = ['a'];
    var aa = ['a', 'a'];
    var abcde = ['a', 'b', 'c', 'd', 'e'];
    var abcdee= ['a', 'b', 'c', 'd', 'e', 'e'];
    var abcdef = ['a', 'b', 'c', 'd', 'e', 'f'];
    var abcdefef = ['a', 'b', 'c', 'd', 'e', 'f', 'e', 'f'];

    it('merkle root of empty list is 32 0s', () => {
        assert.ok(merkleRoot([]).equals(Buffer.alloc(32)));
    })

    it('merkle root of single element', () => {
        assert.ok(merkleRoot(a).equals(stringToHash('a')));
    })

    it('merkle root collisions', function () {
        checkDifferent(a, aa);
        checkDifferent(abcde, abcdee);
        checkDifferent(abcdef, abcdefef);
    })

    it('merkle proof throws on empty list', () => {
        assert.throws(() => {util.merklePath([], stringToHash('a'))}, Error);
    })

    it('merkle proof throws if tx not exist in list', () => {
        assert.throws(() => {util.merklePath(abcdee, stringToHash('f'))}, Error);
    })

    it('merkle proof structure', () => {
        var path = util.merklePath(hashList(abcde), stringToHash('e'));
        assert.equal(path.length, 3);
        assert.equal(path[0].side, 1); // right
        assert.equal(path[1].side, 1); // right
        assert.equal(path[2].side, 0); // left
        assert.ok(util.validateMerklePath(path, stringToHash('e'), merkleRoot(abcde)));
        assert.ok(!util.validateMerklePath(path, stringToHash('c'), merkleRoot(abcde)));
    })

    function testPath(stringList, stringToProve) {
        var path = util.merklePath(hashList(stringList), stringToHash(stringToProve));
       // console.log(path);
        assert.ok(util.validateMerklePath(path, stringToHash(stringToProve), merkleRoot(stringList)),
            `validation failed for txs ${stringList} and tx ${stringToProve}`);
    }


    this.timeout(400000);
    it ('merkle path to size 20', () => {
        var txs = []
        for (var i = 1; i < 20; i++) {
            txs.push(''+i);
            for (var j = 1; j <= i; j++) {
                testPath(txs, ''+j);
            }
        }
    })

    it ('dummy', () => {
        testPath(['1', '2', '3'], '1');
    });

    it ('merkle path negative test wrong side', () => {
        var path = util.merklePath(hashList(abcde), stringToHash('d'));
        path[1].side = 1; // Flip side of one path component
        assert.ok(!util.validateMerklePath(path, stringToHash('d'), merkleRoot(abcde)));
    });

    it ('merkle path negative test wrong hash', () => {
        var path = util.merklePath(hashList(abcde), stringToHash('d'));
        path[2].hash = stringToHash('invalid'); // wrong hash
        assert.ok(!util.validateMerklePath(path, stringToHash('d'), merkleRoot(abcde)));
    });

    it ('randomBytes returns random bytes, different every time', () => {
        var bytes1 = util.randomBytes(40);
        assert.equal(40, bytes1.length);
        var bytes2 = util.randomBytes(40);
        assert.equal(40, bytes2.length);
        assert.ok(!bytes1.equals(bytes2));
    });

    it ('randomBytes returns empty buffer on size 0', () => {
       assert.ok(Buffer.of(0), util.randomBytes(0));
    });

    it ('randomBytes throws on negative size', () => {
        assert.throws(() => {util.randomBytes(-1)}, Error);
    });

    it ('randomBytes on non-integer round up to closer lower integer', () => {
        assert.equal(3, util.randomBytes(3.14).length);
    });

    it ('randomBytes throws on non-number', () => {
        assert.throws(() => {util.randomBytes("32")}, Error);
    });

    it ('createPublicKey creates a valid secp256k1 pubkey', () => {
        var privKey = Buffer.from("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", 'hex');
        var pubKey = util.createPublicKey(privKey);
        var signature = util.sign("hello", privKey);
        assert.ok(util.checkSignature("hello", pubKey, signature));
    });

    it ('createPublicKey throws on invalid privKey', () => {
        assert.throws(() => {util.createPublicKey(-1)});
        assert.throws(() => {util.createPublicKey(util.randomBytes(0))});
        assert.throws(() => {util.createPublicKey(util.randomBytes(31))});
        assert.throws(() => {util.createPublicKey(util.randomBytes(33))});
    });

    it('converts hex to Buffer', () => {
        var pubkey = "031b84c5567b126440995d3ed5aaba0565d71e1834604819ff9c17f5e9d5dd078f";
        var bufferedPubKey = util.toBuffer(pubkey);
        assert.equal(bufferedPubKey.toString('hex'), pubkey)
    });

    it('makes a tuid', () => {
        var tuid = util.makeTuid();
        assert.equal(util.toBuffer(tuid).length, 16);
        assert.equal(tuid.length, 32);
    });

    it('generates key pairs', () => {
       var user = util.makeKeyPair();
       assert.ok(user.privKey);
       assert.ok(user.pubKey);
    });

    it("verifies the authenticity of a private key", () => {
        var user = {
            privKey: util.toBuffer("0101010101010101010101010101010101010101010101010101010101010101"),
            pubKey: util.toBuffer("031b84c5567b126440995d3ed5aaba0565d71e1834604819ff9c17f5e9d5dd078f"),
        };
        var verifiedUser = util.verifyKeyPair(util.toBuffer("0101010101010101010101010101010101010101010101010101010101010101"));
        assert.deepStrictEqual(verifiedUser, user);
    });
});
