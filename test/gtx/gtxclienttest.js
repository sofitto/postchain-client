const apiPort = 7740;
const blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";

let crypto = require('crypto');
let secp256k1 = require('secp256k1');

// Create some dummy keys
let signerPrivKeyA = Buffer.alloc(32, 'a');
let signerPubKeyA = secp256k1.publicKeyCreate(signerPrivKeyA);

let signerPrivKeyB = Buffer.alloc(32, 'b');
let signerPubKeyB = secp256k1.publicKeyCreate(signerPrivKeyB);


// The lower-level client that can be used for any
// postchain client messages. It only handles binary data.
let restClient = require('../../index').restClient;

// The higher-level client that is used for generic transactions, GTX.
// This utilizes the GTX format described below to pass function calls
// from the client to the postchain backend implementation.
let gtxClient = require('../../index').gtxClient;

// Create an instance of the rest client and configure it for a specific
// base url. You may set an optional pool size for the connection pool.
// Default pool size is 10. Applications that do hundreds of requests
// per second may consider setting this a bit higher, for example 100.
// It *may* speed things up a bit.
let rest = restClient.createRestClient(`http://localhost:${apiPort}`, blockchainRID, 5);

// Each blockchain has a blockchainRID, that identifies the blockchain
// we want to work with. This blockchainRID must match the blockchain RID
// encoded into the first block of the blockchain. How the blockchainRID
// is constructed is up to the creator of the blockchain. In this example
// we use the linux command:
// echo "A blockchain example"| sha256sum
let hexBlockchainRID = Buffer.from(blockchainRID, 'hex');

// Create an instance of the higher-level gtx client. It will
// use the rest client instance and it will allow calls to functions
// fun1 and fun2. The backend implementation in Postchain must
// provide those functions.
let gtx = gtxClient.createClient(rest, hexBlockchainRID, ['gtx_test']);

// Start a new request. A request instance is created.
// The public keys are the keys that must sign the request
// before sending it to postchain. Can be empty.
let req = gtx.newTransaction([signerPubKeyA]);

// call fun1 with three arguments: a string, an array and a Buffer
req.gtx_test(1, 'arg1');
// req.gtx_test(2, Buffer.from('apa', 'utf8'));
// req.gtx_test(3, []);
// req.gtx_test(4, {});
// req.gtx_test(5, null);
// req.gtx_test(6, 10)
// req.gtx_test(7, [[], 'a', {b: [1, 2, null]}, 0, 10, {}, Buffer.from('apa', 'utf8'), null])

req.sign(signerPrivKeyA, signerPubKeyA);

const txRID = req.getTxRID();


req.postAndWaitConfirmation().then( ()=> {
    const req2 = gtx.newTransaction([signerPubKeyB]);
    req2.gtx_test(1, 'rejectMe');
    req2.sign(signerPrivKeyB, signerPubKeyB);
    return req2.postAndWaitConfirmation()
}).catch((e) => console.log(e));

// Now we can query Postchain. The backend must have a method
// query method named "findStuff" (readOnlyConn, queryObject) that can
// understand the query object and typically perform a search using
// the database connection readOnlyConn. The backend query function
// can return any serializable result object you chose
// let queryObject = {type: "findStuff", text: 'arg1'};
// let resultHandler = (error, result) => {
//     if (error) {
//         console.error(error);
//         return;
//     }
//     if (result.hits == 0) {
//         // Poll every 2 seconds
//         setTimeout(gtx.query(queryObject, resultHandler), 2000);
//     }
//     console.log(JSON.stringify(result));
// }
// gtx.query(queryObject, resultHandler);
//
// function sha256(buffer) {
//     return crypto.createHash('sha256').update(buffer).digest()
// }