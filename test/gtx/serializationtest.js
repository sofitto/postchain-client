var assert = require('chai').assert;
var serialization = require('../../src/gtx/serialization');

var sigs = require('../signatures');
var signatures = sigs.signatures;
var signers = sigs.signers;

describe('Encode-decode tests', () => {

    function test(object) {
        var buffer = serialization.encode(object);
        console.log(`Encoded buffer: ${buffer.toString('hex')}`);
        assert.deepEqual(serialization.decode(buffer), object);
    }

    function tx(calls, signers, signatures) {
        var sigs = signatures
        if (!signatures) {
            sigs = []
        }
        return {blockchainRID: Buffer.from('apa'), operations: calls, signers: signers, signatures: sigs}
    }

    it('empty call should be ok', () => {
        test(tx([], [], []));
    })

    it('empty call with one signature', () => {
        test(tx([], [], signatures(1)));
    })

    it('single call with no param', () => {
        test(tx([{opName: 'func1åäö', args: []}], [], signatures(1)));
    })

    it('single call with no signatures ok', () => {
        // This is only used when signing and validating signatures.
        // signatures must not be present in the signed data.
        test(tx([{opName: 'func1åäö', args: []}], signers(2)));
    })

    function testSingleArg(arg) {
        test(tx([{opName: 'f', args: [arg]}], [], []));
    }

    it('single call with single integer param', () => {
        testSingleArg(1);
    })

    xit('single call with single negative integer param', () => {
        testSingleArg(-1);
    })

    function testSingleArgAssertFail(value) {
        try {
            testSingleArg(value);
        } catch (error) {
            console.log(error);
            assert.ok(error instanceof Error);
            return;
        }
        assert.fail();
    }

    it('zero integer param', () => {
        testSingleArg(0);
    })

    xit('minimum integer param', () => {
        testSingleArg(-Math.pow(2, 47));
    })

    it('maximum integer param', () => {
        testSingleArg(Number.MAX_SAFE_INTEGER);
    })

    it('single call with single maximum integer param', () => {
        testSingleArg(0);
    })

    it('single call with single string param', () => {
        testSingleArg('hello');
    })

    it('single call with single buffer param', () => {
        testSingleArg(Buffer.from('hello', 'utf8'));
    })

    it('single call with empty array param', () => {
        testSingleArg([]);
    })

    it('single call with single array param', () => {
        testSingleArg(['a']);
    })

    it('single call with empty object param,', () => {
        testSingleArg({})
    })

    it('single call with single property object param,', () => {
        testSingleArg({a: "b"})
    })

    it('single call with two property object param,', () => {
        testSingleArg({a: 'a string', b: 2})
    })

    it('single call with nested property object param,', () => {
        testSingleArg({a: {c: {d: "d string"}, e: [Buffer.from("hello")]}, b: 2})
    })


    it('single call with multiple mixed type param', () => {
        test(tx([{
                opName: 'func1åäö',
                args: ['abc åäö€', 1,
                    ['', 1, 0, ['a', 'b'], Buffer.from('hi', 'utf8')],
                    Buffer.from('hello', 'utf8'), [], {}, Buffer.alloc(0)]
            }], [], []));
    })

    it('multiple calls with multiple mixed type param', () => {
        test(tx([{opName: 'func0', args: []},
                {opName: 'func1', args: [1, 'a', ['c', 'd'], [1], Buffer.alloc(2, 3)]},
                {
                    opName: 'func3åäö',
                    args: ['abc åäö€', 1,
                        ['', 1, 0, ['a', 'b'], Buffer.from('hi', 'utf8')],
                        Buffer.from('hello', 'utf8'), {}, [], Buffer.alloc(0)]
                }], [], []));
    })

    it('multiple calls with multiple signatures', () => {
        test(tx([{opName: 'func1åäö', args: ['abc åäö€', 1]}], signers(3), signatures(4)));
    });
});
