var restClient = require('../index').restClient;
var express = require("express");
var assert = require("assert");
var bodyParser = require("body-parser");
describe('Rest client tests', function () {
    const port = 49545
    var client = restClient.createRestClient('http://localhost:' + port + '/basepath/');

    const statusUnknown   = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    const statusRejected  = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
    const statusConfirmed = "cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
    const statusNotFound  = "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"
    const statusWaiting   = "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"


    function clientExpectResponse(shouldBeError, expectedResponseObject, done) {
        return (error, responseObject) => {
            if (shouldBeError) assert.ok(error != null)
            else assert.ok(error == null);
            if (expectedResponseObject === null) {
                assert.ok(responseObject === null);
            } else {
                assert.deepEqual(responseObject, expectedResponseObject);
            }
            done();
        }
    }

    function testQuery(requestObject, answerWithCode, answerWithResponseObject, done) {
        client.query(requestObject, clientExpectResponse(answerWithCode != 200, answerWithResponseObject, done));
    }

    it('Query OK null response', done => {
        testQuery({'a':'oknullresponse', 'c':3}, 200, null, done);
    });

    it('Query OK empty response', done => {
        testQuery({'a':'okemptyresponse', 'c':3}, 200, {}, done);
    });

    it('Query OK', done => {
        testQuery({'a':'oksimpleresponse', 'c':3}, 200, {test: 'hi'}, done);
    });

    it('Query error 400 response', done => {
        testQuery({'a':'usermistake', 'c':3}, 400, {error: "expected error"}, done);
    });

    it('Query error 500 response', done => {
        testQuery({'a':'programmermistake', 'c':3}, 500, {error: "expected error"}, done);
    });

    function testPostTransaction(txBuffer, answerWithCode, answerWithResponseObject, done) {
        client.postTransaction(txBuffer, clientExpectResponse(answerWithCode != 200, answerWithResponseObject, done));
    }

    it('postTransaction error 400 response', done => {
        testPostTransaction(Buffer.from('hello400'), 400, {error: "expected error"}, done);
    });

    it('postTransaction error 500 response', done => {
        testPostTransaction(Buffer.from('hello500'), 500, {error: "expected error"}, done);
    });

    it('postTransaction error OK', done => {
        testPostTransaction(Buffer.from('helloOK'), 200, {}, done);
    });

    function testGetTransaction(txHashBuffer, answerWithCode, serverReturnTx, done) {
        client.getTransaction(txHashBuffer, clientExpectResponse(answerWithCode != 200 && answerWithCode != 404, serverReturnTx, done));
    }

    it('getTransaction OK', done => {
        testGetTransaction(b(statusConfirmed), 200, Buffer.from('1234', 'hex'), done);
    });

    it('getTransaction 404', done => {
        testGetTransaction(b(statusUnknown), 404, null, done);
    });

    it('getTransaction malformed hash throws', () => {
        try {
            client.getTransaction(Buffer.alloc(31), () => {
            });
        } catch (error) {
            return;
        }
        ;
        assert.fail();
    });

    // it('getConfirmationProof OK', done => {
    //     var txHash = Buffer.alloc(32, 1);
    //     var header = Buffer.alloc(72, 2);
    //     var pubKeyA = Buffer.alloc(33, 3);
    //     var pubKeyB = Buffer.alloc(33, 4);
    //     var signatureA = Buffer.alloc(50, 3);
    //     var signatureB = Buffer.alloc(50, 4);
    //
    //     var txHashLevel3 = Buffer.alloc(32, 3);
    //     var txHashLevel2 = Buffer.alloc(32, 2);
    //     var txHashLevel1 = Buffer.alloc(32, 1);
    //
    //     var serverReturnProof = {
    //         hash: h(txHash), // To make it self contained
    //         blockHeader: h(header),
    //         signatures: [{pubKey: h(pubKeyA), signature: h(signatureA)},
    //             {pubKey: h(pubKeyB), signature: h(signatureB)}],
    //         merklePath: [{side: 0, hash: h(txHashLevel3)},
    //             {side: 1, hash: h(txHashLevel2)},
    //             {side: 1, hash: h(txHashLevel1)}]
    //     };
    //
    //
    //     var SUTresult = {
    //         hash: txHash, // To make it self contained
    //         blockHeader: header,
    //         signatures: [{pubKey: pubKeyA, signature: signatureA},
    //             {pubKey: pubKeyB, signature: signatureB}],
    //         merklePath: [{side: 0, hash: txHashLevel3},
    //             {side: 1, hash: txHashLevel2},
    //             {side: 1, hash: txHashLevel1}]
    //     };
    //
    //     client.getConfirmationProof(txHash, clientExpectResponse(false, SUTresult, done));
    // });

    it('getConfirmationProof 404', done => {
        client.getConfirmationProof(b(statusUnknown), clientExpectResponse(false, null, done));
    });

    function testGetStatus(txHashBuffer, answerWithCode, serverReturnObject, done) {
        client.status(txHashBuffer, clientExpectResponse(answerWithCode != 200, serverReturnObject, done));
    }

    it('status unknown', done => {
        testGetStatus(b(statusUnknown), 200, {status: 'unknown'}, done);
    })

    it('status confirmed', done => {
        testGetStatus(b(statusConfirmed), 200, {status: 'confirmed'}, done);
    })


    it('status waiting', done => {
        testGetStatus(b(statusWaiting), 200, {status: 'waiting'}, done);
    })


    it('status rejected', done => {
        testGetStatus(b(statusRejected), 200, {status: 'rejected'}, done);
    })

    it('status short hash', () => {
        try {
            client.status(txHashBuffer, () => {
                assert.fail()
            });
        } catch (error) {
            return;
        }
        assert.fail();
    })

    function h(buffer) {
        return buffer.toString('hex');
    }
    function b(hex) {
        return Buffer.from(hex, 'hex');
    }
});
