var restClient = require('../index').restClient;
var express = require("express");
var assert = require("assert");
var bodyParser = require("body-parser");
describe('Rest client tests', function () {
    var app;
    var client;
    var blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";

    beforeEach(function (done) {
        app = express();
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: true}));
        var server = app.listen(0, () => {
            var port = server.address().port;
            client = restClient.createRestClient('http://localhost:' + port, blockchainRID, 5);
            done();
        });
    });

    function serverExpectPost(path, requestObject, responseCode, responseBody) {
        app.post(path, (req, res) => {
            assert.deepEqual(req.body, requestObject);
            if (responseBody) {
                res.status(responseCode).send(responseBody);
            } else {
                res.status(responseCode).send();
            }
        });
    }

    function serverExpectGet(path, responseCode, responseBody) {
        app.get(path, (req, res) => {
            if (responseBody) {
                res.status(responseCode).send(responseBody);
            } else {
                res.status(responseCode).send();
            }
        });
    }

    function clientExpectResponse(shouldBeError, expectedResponseObject, done) {
        return (error, responseObject) => {
            if (shouldBeError) assert.ok(error != null)
            else assert.ok(error == null);
            if (expectedResponseObject === null) {
                assert.ok(responseObject === null);
            } else {
                assert.deepEqual(responseObject, expectedResponseObject);
            }
            done();
        }
    }

    function testQuery(requestObject, answerWithCode, answerWithResponseObject, done) {
        serverExpectPost(`/query/${blockchainRID}`, requestObject, answerWithCode, answerWithResponseObject);
        client.query(requestObject, clientExpectResponse(answerWithCode != 200, answerWithResponseObject, done));
    }

    this.timeout(100000);
    it('Test 10000 simultaneous requests', done => {
        // This test is making sure that connection pooling works well.
        app.post(`/query/${blockchainRID}`, (req, res) => {
            assert.deepEqual(req.body, {type: "aQueryType"});
            setTimeout(() => res.status(200).send({status: "OK"}), 10);
        });

        let callCount = 10000;
        let responseCount = 0;
        for (let i = 0; i < callCount; i++) {
            client.query("aQueryType", {}, (error, responseObject) => {
                assert.ok(error == null);
                assert.deepEqual(responseObject, {status: "OK"});
                responseCount++;
                if (responseCount === callCount) {
                    done();
                }
            });
        }
    });

    it('Query error 400 response', done => {
        testQuery("aQueryType", {}, 400, {error: "test error"}, done);
    });

    it('Query OK null response', done => {
        testQuery("aQueryType", {}, 200, null, done);
    });

    it('Query OK empty response', done => {
        testQuery("", {}, 200, {}, done);
    });

    it('Query OK', done => {
        testQuery("aQueryType", {}, 200, {test: 'hi'}, done);
    });

    function testPostTransaction(txBuffer, answerWithCode, answerWithResponseObject, done) {
        serverExpectPost(`/tx/${blockchainRID}`, {tx: txBuffer.toString('hex')}, answerWithCode, answerWithResponseObject);
        client.postTransaction(txBuffer, clientExpectResponse(answerWithCode != 200, answerWithResponseObject, done));
    }

    it('postTransaction error 400 response', done => {
        testPostTransaction(Buffer.from('hello'), 400, {error: "test error"}, done);
    });

    it('postTransaction error 500 response', done => {
        testPostTransaction(Buffer.from('hello'), 500, {error: "test error"}, done);
    });

    it('postTransaction error OK', done => {
        testPostTransaction(Buffer.from('hello'), 200, {}, done);
    });

    function testGetTransaction(txHashBuffer, answerWithCode, serverReturnTx, done) {
        serverExpectGet(`/tx/${blockchainRID}/` + txHashBuffer.toString('hex'), answerWithCode, serverReturnTx ? {tx: serverReturnTx.toString('hex')} : null);
        client.getTransaction(txHashBuffer, clientExpectResponse(answerWithCode != 200 && answerWithCode != 404, serverReturnTx, done));
    }

    it('getTransaction OK', done => {
        testGetTransaction(Buffer.alloc(32), 200, Buffer.from('a transaction'), done);
    });

    it('getTransaction 404', done => {
        testGetTransaction(Buffer.alloc(32), 404, null, done);
    });

    it('getTransaction malformed hash throws', () => {
        try {
            client.getTransaction(Buffer.alloc(31), () => {
            });
        } catch (error) {
            return;
        }
        ;
        assert.fail();
    });

    it('getConfirmationProof OK', done => {
        var txHash = Buffer.alloc(32, 1);
        var header = Buffer.alloc(72, 2);
        var pubKeyA = Buffer.alloc(33, 3);
        var pubKeyB = Buffer.alloc(33, 4);
        var signatureA = Buffer.alloc(50, 3);
        var signatureB = Buffer.alloc(50, 4);

        var txHashLevel3 = Buffer.alloc(32, 3);
        var txHashLevel2 = Buffer.alloc(32, 2);
        var txHashLevel1 = Buffer.alloc(32, 1);

        var serverReturnProof = {
            hash: h(txHash), // To make it self contained
            blockHeader: h(header),
            signatures: [{pubKey: h(pubKeyA), signature: h(signatureA)},
                {pubKey: h(pubKeyB), signature: h(signatureB)}],
            merklePath: [{side: 0, hash: h(txHashLevel3)},
                {side: 1, hash: h(txHashLevel2)},
                {side: 1, hash: h(txHashLevel1)}]
        };

        serverExpectGet(`/tx/${blockchainRID}/` + txHash.toString('hex') + '/confirmationProof', 200, serverReturnProof);

        var SUTresult = {
            hash: txHash, // To make it self contained
            blockHeader: header,
            signatures: [{pubKey: pubKeyA, signature: signatureA},
                {pubKey: pubKeyB, signature: signatureB}],
            merklePath: [{side: 0, hash: txHashLevel3},
                {side: 1, hash: txHashLevel2},
                {side: 1, hash: txHashLevel1}]
        };

        client.getConfirmationProof(txHash, clientExpectResponse(false, SUTresult, done));
    });

    it('getConfirmationProof 404', done => {
        let txHash = Buffer.alloc(32, 1);
        serverExpectGet(`/tx/${blockchainRID}/` + h(txHash) + '/confirmationProof', 404, null);
        client.getConfirmationProof(txHash, clientExpectResponse(false, null, done));
    });

    function testGetStatus(txHashBuffer, answerWithCode, serverReturnObject, done) {
        serverExpectGet(`/tx/${blockchainRID}/` + txHashBuffer.toString('hex') + '/status', answerWithCode, serverReturnObject);
        client.status(txHashBuffer, clientExpectResponse(answerWithCode != 200, serverReturnObject, done));
    }

    it('status unknown', done => {
        testGetStatus(Buffer.alloc(32, 1), 200, {status: 'unknown'}, done);
    })

    it('status confirmed', done => {
        testGetStatus(Buffer.alloc(32, 1), 200, {status: 'confirmed'}, done);
    })

    it('status short hash', () => {
        try {
            client.status(txHashBuffer, () => {
                assert.fail()
            });
        } catch (error) {
            return;
        }
        assert.fail();
    })

    function h(buffer) {
        return buffer.toString('hex');
    }
});
