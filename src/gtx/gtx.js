var serialization = require('./serialization');
var util = require('../util');

module.exports.emptyGtx = function (blockchainRID) {
    return {blockchainRID: blockchainRID, operations: [], signers: []};
}

/**
 * Adds a function call to a GTX. Creates a new GTX if none specified.
 * This function will throw Error if gtx is already signed
 * @param opName the name of the function to call
 * @param args the array of arguments of the function call. If no args, this must be an empty array
 * @param gtx the function call will be added to this gtx
 * @returns the gtx
 * @throws if gtx is null or if gtx is already signed
 */
module.exports.addTransactionToGtx = function (opName, args, gtx) {
    if (gtx == null) {
        throw new Error("No Gtx to add operation to")
    }
    if (gtx.signatures) {
        throw new Error("Cannot add function calls to an already signed gtx");
    }
    gtx.operations.push({opName: opName, args: args});
    return gtx;
}

module.exports.addSignerToGtx = function (signer, gtx) {
    if (gtx.signatures) {
        throw new Error("Cannot add signers to an already signed gtx");
    }
    gtx.signers.push(signer);
}

/**
 * Serializes the gtx for signing
 * @param gtx the gtx to serialize
 */
module.exports.getBufferToSign = function (gtx) {
    // console.log("Buffer to sign: " + JSON.stringify({calls: gtx.calls, signers: gtx.signers}));

    return serialization.encode({blockchainRID: gtx.blockchainRID, operations: gtx.operations,
        signers: gtx.signers, signatures: []});
}

/**
 * Signs the gtx with the provided privKey. This is a convenience function
 * for situations where you don't have to ask someone else to sign.
 */
module.exports.sign = function(privKey, pubKey, gtx) {
    var bufferToSign = module.exports.getBufferToSign(gtx);
    var signature = util.sign(bufferToSign, privKey);
    console.log("PubKey: " + pubKey.toString('hex'));
    console.log("Signature: " + signature.toString('hex'));
    module.exports.addSignature(pubKey, signature, gtx);
}

/**
 * Adds a signature to the gtx
 */
module.exports.addSignature = function(pubKeyBuffer, signatureBuffer, gtx) {
    if (!gtx.signatures) {
        gtx.signatures = Array(gtx.signers.length).fill(null);
    }
    if (gtx.signers.length != gtx.signatures.length) {
        throw new Error("Mismatching signers and signatures");
    }
    var signerIndex = gtx.signers.findIndex((signer) => pubKeyBuffer.equals(signer));
    if (signerIndex === -1) {
        throw new Error("No such signer, remember to call addSignerToGtx() before adding a signature");
    }
    gtx.signatures[signerIndex] = signatureBuffer;
}

module.exports.serialize = function(gtx) {
    if (!gtx.signatures) {
        // The gtx is not signed, but we must include
        // the signatures attribute, so let's add that.
        gtx.signatures = [];
    }
    return serialization.encode(gtx);
}

module.exports.deserialize = function(gtxBytes) {
    return serialization.decode(gtxBytes);
}

module.exports.encodeValue = serialization.encodeValue;
module.exports.decodeValue = serialization.decodeValue;
