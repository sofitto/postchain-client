"use strict";
var gtxTool = require('./gtx');
var secp256k1 = require('secp256k1');
var util = require('../util');

module.exports.createClient = function (restApiClient, blockchainRID, functionNames) {
    functionNames.push('message');

    const client = {
        newTransaction: function (signers) {
            const newGtx = gtxTool.emptyGtx(blockchainRID);
            signers.forEach(signer => gtxTool.addSignerToGtx(signer, newGtx));

            const req = {
                gtx: newGtx,

                sign: function (privKey, pubKey) {
                    let pub = pubKey;
                    if (!pub) {
                        pub = secp256k1.publicKeyCreate(privKey);
                    }
                    gtxTool.sign(privKey, pub, this.gtx);
                },

                getTxRID: function () {
                    return util.sha256(req.getBufferToSign());
                },

                getBufferToSign: function () {
                    return gtxTool.getBufferToSign(this.gtx);
                },

                addSignature: function (pubKey, signature) {
                    gtxTool.addSignature(pubKey, signature, this.gtx);
                },

                // raw call
                addOperation: function (name, ...args) {
                    gtxTool.addTransactionToGtx(name, args, this.gtx);
                },

                postAndWaitConfirmation() {
                    return restApiClient.postAndWaitConfirmation(
                        gtxTool.serialize(this.gtx), this.getTxRID()
                    )
                },

                send: function (callback) {
                    var gtxBytes = gtxTool.serialize(this.gtx);
                    restApiClient.postTransaction(gtxBytes, callback);
                    this.gtx = null;
                    this.gtxBytes = gtxBytes;
                }
            };

            functionNames.forEach((functionName) => {
                req[functionName] = function (...args) {
                    gtxTool.addTransactionToGtx(functionName, args, this.gtx);
                }
            });
            return req;
        },

        /*
        This function is only used for the messaging feature. It's a convenience function that
        uses POST /query under the hood.
         */
        pollMessages: function (recipient, fromMessageId, maxHits, resultCallback) {
            let queryObject = {type: 'getGtxMessages',
                recipient: recipient.toString('hex'),
                sinceMessageId: fromMessageId == null ? null : fromMessageId.toString('hex'),
                maxHits: maxHits};
            restApiClient.query(queryObject, (error, messages) => {
                if (error) {
                    resultCallback(error);
                    return;
                }
                var result = messages.map((gtxAndIndex) => {
                    console.log("gtxData: " + JSON.stringify(gtxAndIndex.txData));
                    console.log("isBuffer: " + Buffer.isBuffer(gtxAndIndex.txData));
                    var txData = Buffer.from(gtxAndIndex.txData, 'hex');
                    var gtx = gtxTool.deserialize(txData);
                    var call = gtx.calls[gtxAndIndex.callIndex];
                    // call args: recipients, 'testtype', messageBytes
                    return {recipients: call.args[0], type: call.args[1],
                        message: call.args[2], messageId: util.hash256(txData)};
                });
                resultCallback(null, result);
            });
        },

        query: function (queryName, queryObject) {
            return restApiClient.query(queryName, queryObject);
        }
    };

    return client;
};
