var asn = require('asn1.js');

var ASNDictPair = asn.define('DictPair', function () {
    this.seq().obj(
        this.key('name').utf8str(),
        this.key('value').use(ASNGTXValue)
    );
});

var ASNGTXValue = asn.define('GtxValue', function () {
    this.choice({
        null: this.explicit(0).null_(),
        byteArray: this.explicit(1).octstr(),
        string: this.explicit(2).utf8str(),
        integer: this.explicit(3).int(),
        dict: this.explicit(4).seqof(ASNDictPair),
        array: this.explicit(5).seqof(ASNGTXValue)
    });
});

var ASNGTXOperation = asn.define('GtxOperation', function () {
    this.seq().obj(
        this.key('opName').utf8str(),
        this.key('args').seqof(ASNGTXValue)
    )
})


var ASNBuffer = asn.define('Buffer', function() {
    this.octstr();
});

var ASNGTXTransaction = asn.define('GtxTransaction', function () {
    this.seq().obj(
        this.key('blockchainRID').octstr(),
        this.key('operations').seqof(ASNGTXOperation),
        this.key('signers').seqof(ASNBuffer),
        this.key('signatures').seqof(ASNBuffer)
    )
})
/*
Messages DEFINITIONS ::= BEGIN

DictPair ::= SEQUENCE {
    name UTF8String,
        value GTXValue
}

GTXValue ::= CHOICE {
        null [0] NULL,
        byteArray [1] OCTET STRING,
        string [2] UTF8String,
        integer [3] INTEGER,
        dict [4] SEQUENCE OF DictPair,
        array [5] SEQUENCE OF GTXValue
}

GTXOperation ::= SEQUENCE {
    opName UTF8String,
        args SEQUENCE OF GTXValue
}

GTXTransaction ::= SEQUENCE {
    blockchainID OCTET STRING,
        operations SEQUENCE OF GTXOperation,
        signers SEQUENCE OF OCTET STRING,
        signatures SEQUENCE OF OCTET STRING
}

END
*/
/**
 * @param bytes the raw client message
 * @returns {calls:      [{functionName: <funcName>, args: [arg1, arg2, ...]}, ...],
 *           signatures: [{pubKey: <buffer>, signature: <buffer>}]}
 */
module.exports.decode = (bytes) => {
    var rawObject = ASNGTXTransaction.decode(bytes);
    rawObject.operations.forEach(operation => {
        operation.args = operation.args.map(arg => parseValue(arg));
    });
    return rawObject;
};

module.exports.encode = (opsAndSigs) => {
    var preparedOps = opsAndSigs.operations.map((op) => {
        return {opName: op.opName,
                args: op.args.map((arg) => createTypedArg(arg))}
    });
    var toEncode = {blockchainRID: opsAndSigs.blockchainRID, operations: preparedOps,
                signers: opsAndSigs.signers, signatures: opsAndSigs.signatures}
    if (toEncode.signatures == null) {
        // use empty signatures
        toEncode.signatures = opsAndSigs.signers.map(function () { return Buffer.alloc(0); })
    }
    return ASNGTXTransaction.encode(toEncode);
};

module.exports.encodeValue = (arg) => {
    return ASNGTXValue.encode(createTypedArg(arg))
};

module.exports.decodeValue = (bytes) => {
    const obj = ASNGTXValue.decode(bytes);
    return parseValue(obj);
};

var BN = require('bn.js')
function parseValue(typedArg) {
    // console.log("Typedarg:", typedArg);
    if (typedArg.type === 'null') {
        return null
    }
    if (typedArg.type === 'byteArray') {
        return typedArg.value
    }
    if (typedArg.type === 'string') {
        return typedArg.value
    }
    if (typedArg.type === 'integer') {
        return typedArg.value.toNumber()
    }
    if (typedArg.type === 'array') {
        return typedArg.value.map(item => parseValue(item));
    }
    if (typedArg.type === 'dict') {
        var result = {}
        typedArg.value.forEach(function (pair) {
            result[pair.name] = parseValue(pair.value);
        })
        return result
    }
    throw new Error(`Cannot parse typedArg ${typedArg}. Unknown type ${typedArg.type}`);
}

function createTypedArg(value) {
    try {
        if (value == null) {
            return {type: 'null', value: null}
        }
        if (Buffer.isBuffer(value)) {
            return {type: 'byteArray', value: value};
        }
        if (typeof value === 'string') {
            return {type: 'string', value: value};
        }
        if (typeof value === 'number') {
            if (!Number.isInteger(value)) {
                throw Error("User error: Only integers are supported");
            }
            return {type: 'integer', value: new BN(value)}
        }
        if (value.constructor === Array) {
            return {type: 'array', value: value.map(item => createTypedArg(item))};
        }
        if (typeof value === 'object') {
            return {type: 'dict', value: Object.keys(value).map(function (key) {
                return {name: key, value: createTypedArg(value[key])}
            })}
        }
    } catch (error) {
        throw new Error(`Failed to encode ${value.toString()}: ${error}`);
    }
    throw new Error(`value ${value} have unsupported type: ${typeof value}`);
}
