"use strict";

var url = require('url');
var rq = require('request');

var separateReqPool = {maxSockets: 100};

module.exports.createRestClient = function (urlBase, blockchainRID, maxSockets) {
    return {
        config: {urlBase: urlBase, pool: {maxSockets: maxSockets ? maxSockets : 10}},

        /**
         * Retrieves the client message with the specified double-sha256 hash
         * @param messageHash A buffer of 32 bytes
         * @param callback parameters (error, serializedMessage) if first
         * parameter is not null, an error occurred.
         * If first parameter is null, then the second parameter is a buffer
         * with the serialized client message. If no such client message exists,
         * the callback will be called with (null, null).
         */
        getTransaction: function (messageHash, callback) {
            validateMessageHash(messageHash)
            get(this.config, 'tx/' + blockchainRID + '/' + messageHash.toString('hex'), (error, statusCode, responseObject) => {
                handleGetResponse(error, statusCode, statusCode === 200 ? Buffer.from(responseObject.tx, 'hex') : null, callback);
            });

        },

        /**
         * Sends a transaction to postchain for inclusion in a block.
         * Use status() to monitor progress once this transaction is
         * posted.
         *
         * @param serializedTransaction The transaction (a buffer) to send
         * @param callback taking parameter (error, responseObject) if error is null
         * then resonseObject is also null. If error is not null, then responseObject
         * is an object with the string property 'error'
         */
        postTransaction: function (serializedTransaction, callback) {
            if (!Buffer.isBuffer(serializedTransaction)) {
                throw new Error("messageHash is not a Buffer");
            }
            var jsonObject = {tx: serializedTransaction.toString('hex')};
            doPost(this.config, 'tx/'+blockchainRID, jsonObject, callback);
        },

        /**
         * Retrieves a confirmation proof for a client message with the specified double-sha256
         * hash.
         * @param messageHash A buffer of 32 bytes
         * @param callback parameters (error, serializedMessage) if first
         * parameter is not null, an error occurred.
         * If first parameter is null, then the second parameter is an object
         * like the following:
         *
         * {hash: messageHashBuffer,
         *  blockHeader: blockHeaderBuffer,
         *  signatures: [{pubKey: pubKeyBuffer, signature: sigBuffer}, ...],
         *  merklePath: [{side: <0|1>, hash: <hash buffer level n-1>},
         *               ...
         *               {side: <0|1>, hash: <hash buffer level 1>}]}
         *
         * If no such client message exists, the callback will be called with (null, null).
         *
         * The proof object can be validated using
         * postchain-common.util.validateMerklePath(proof.merklePath, proof.hash,
         * proof.blockHeader.slice(32, 64))
         *
         * The signatures must be validated agains some know trusted source for valid signers
         * at this specific block height.
         */
        getConfirmationProof: function (messageHash, callback) {
            validateMessageHash(messageHash);
            get(this.config, 'tx/'+ blockchainRID + '/' + messageHash.toString('hex') + '/confirmationProof', (error, statusCode, responseObject) => {
                if (statusCode === 200) {
                    responseObject.hash = b(responseObject.hash);
                    responseObject.blockHeader = b(responseObject.blockHeader);
                    if (responseObject.signatures) {
                        responseObject.signatures = responseObject.signatures.map(item => {
                            return {pubKey: b(item.pubKey), signature: b(item.signature)};
                        });
                    }
                    if (responseObject.merklePath) {
                        responseObject.merklePath = responseObject.merklePath.map(item => {
                            return {side: item.side, hash: b(item.hash)};
                        });
                    }
                }
                handleGetResponse(error, statusCode, responseObject, callback);
            });
        },

        /**
         * Queries the status of a certain transaction.
         * @param messageHash
         * @param callback taking parameters (error, responseObject). If error is null
         * then responseObject is an object on the form
         * { status: '<confirmed|waiting|rejected|unknown>' }
         * If error is not null, then responseObject
         * is an object with the string property 'error'
         */
        status: function (messageHash, callback) {
            validateMessageHash(messageHash);
            get(this.config, 'tx/' + blockchainRID + '/' + messageHash.toString('hex') + '/status', (error, statusCode, responseObject) => {
                handleGetResponse(error, statusCode, responseObject, callback);
            });
        },

        /**
         * Sends a custom query that the backend implementation interprets and responds to.
         * @param queryObject
         * @param callback taking parameters (error, responseObject). If error is null
         * then responseObject is an object as returned by the backend implementation.
         * If error is not null, then responseObject is an object with the string property 'error'
         */
        query: function (queryName, queryObject) {
            queryObject.type = queryName;

            return new Promise( (resolve, reject) => {
                doPost(this.config, `query/${blockchainRID}`, queryObject, (error, result) =>{
                    if(error) reject(error);
                    else {
                        resolve(result)
                    }
                })
            });
        },

        waitConfirmation(txRID) {
            return new Promise((resolve, reject) => {
                this.status(txRID, (err, res) => {
                    if (err) resolve(err);
                    else {
                        const status = res.status;
                        switch (status) {
                            case "confirmed": resolve(null); break;
                            case "rejected": reject(Error("Message was rejected")); break;
                            case "unknown": reject(Error("Server lost our message")); break;
                            case "waiting":
                                setTimeout(
                                    () => resolve(this.waitConfirmation(txRID)),
                                    511
                                );
                                break;
                            default:
                                console.log(status);
                                reject(Error("got unexpected response from server"));
                        }
                    }
                });
            })
        },

        postAndWaitConfirmation(serializedTransaction, txRID, validate) {
            if (validate === true) return Promise.reject("Automatic validation is not yet implemented");

            return new Promise( (resolve, reject) => {
                this.postTransaction(serializedTransaction, (err) => {
                    if (err) reject(err);
                    else {
                        resolve(this.waitConfirmation(txRID));
                    }
                })

            })
        }
    };
};

function doPost(config, path, jsonObject, responseCallback) {
    post(config, path, jsonObject, (error, statusCode, responseObject) => {
        if (error) {
            console.log("In restclient doPost().", error);
            responseCallback(error);
        } else if (statusCode != 200) {
            console.log(`Unexpected status code from server: ${statusCode}`);
            responseCallback(new Error(`Unexpected status code from server: ${statusCode}`), responseObject);
        } else {
            try {
                console.log(`Ok calling responseCallback with responseObject: ${JSON.stringify(responseObject)}`);
                responseCallback(null, responseObject);
            } catch (error) {
                console.error("restclient.doPost(): Failed to call callback function", error);
            }
        }
    });
}

function get(config, path, callback) {
    console.log("GET URL " + url.resolve(config.urlBase, path));
    var options = {
        method: 'GET', url: url.resolve(config.urlBase, path),
        json: true, pool: config.pool
    };
    rq(options, function (err, response, body) {
        if (err) callback(err);
        else {
            try {
                console.log("BODY", body);
                console.log(typeof body);
                callback(null, response.statusCode, body);
            } catch (e) {
                console.error("restclient.get(): Failed to call callback function", e);
            }
        }
    });
}

function post(config, path, jsonBody, callback) {
    console.log("POST URL " + url.resolve(config.urlBase, path));
    let options = {
        method: 'POST', url: url.resolve(config.urlBase, path),
        json: true, body: jsonBody, pool: config.pool
    };
    rq(options, function (err, response, body) {
        if (err) callback(err);
        else {
            try {
                console.log(body);
                console.log(typeof body);
                callback(null, response.statusCode, body ? body : null);
            } catch (e) {
                callback(e);
            }
        }
    })

}

function validateMessageHash(messageHash) {
    if (!Buffer.isBuffer(messageHash)) {
        throw new Error("messageHash is not a Buffer");
    }
    if (messageHash.length != 32) {
        throw new Error("expected length 32 of messageHash, but got " + messageHash.length);
    }
}

function handleGetResponse(error, statusCode, responseObject, callback) {
    if (error) {
        callback(error);
    } else if (statusCode === 404) {
        console.log("404 received");
        callback(null, null);
    } else if (statusCode != 200) {
        callback(new Error("Unexpected status code from server: " + statusCode));
    } else {
        try {
            callback(null, responseObject);
        } catch (error) {
            console.error("restclient.handleGetResponse(): Failed to call callback function", error);
        }
    }
}

function b(stringValue) {
    if (typeof stringValue === 'string') {
        return Buffer.from(stringValue, 'hex');
    }
    return stringValue;
}

