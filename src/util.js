"use strict";

var crypto = require('crypto');

var secp256k1 = require('secp256k1');

module.exports = {
    sha256: sha256,
    hash256: sha256,
    pgBytes: pgBytes,
    calculateMerkleRoot: calculateMerkleRoot,
    merklePath: merklePath,
    validateMerklePath: validateMerklePath,
    checkSignature: checkSignature,
    checkDigestSignature: checkDigestSignature,
    sign: sign,
    signDigest: signDigest,
    createPublicKey: createPublicKey,
    randomBytes: randomBytes,
    makeKeyPair: makeKeyPair,
    makeTuid: makeTuid,
    toBuffer: toBuffer,
    verifyKeyPair: verifyKeyPair,
};

function createPublicKey(privateKey) {
    if (!Buffer.isBuffer(privateKey) || privateKey.length != 32) {
        throw "privateKey must be a Buffer of 32 bytes"
    }
    return secp256k1.publicKeyCreate(privateKey, true);
}

function randomBytes(size) {
    return crypto.randomBytes(size)
}

function sha256(buffer) {
    return crypto.createHash('sha256').update(buffer).digest()
}

const hash256 = sha256;

function pgBytes(buffer) {
    if (!Buffer.isBuffer(buffer)) {
        throw new Error("util.pgBytes expects a buffer, but got " + typeof buffer);
    }
    return '\\x' + buffer.toString('hex')
}

function reverseBytes(buffer) {
    var result = Buffer.alloc(buffer.length);
    for (var i = 0; i < buffer.length; i++) {
        result[i] = buffer[buffer.length - 1 - i];
    }
    return result;
}

var internalNodePrefix = Buffer.alloc(1, 0);
var leafPrefix = Buffer.alloc(1, 1);
var nonExistingNodePrefix = Buffer.alloc(1, 2);
var nonExistingNodeHash = Buffer.alloc(32);

function hashConcat(items) {
    return hash256(Buffer.concat(items));
}

function calculateMerkleRoot(hashes, depth, leafDepth) {
    var numTransactions = hashes.length;
    if (numTransactions === 0) {
        return Buffer.alloc(32); // Just zeros
    }

    if (depth === undefined) depth = 0;
  
    if (!leafDepth) {
        leafDepth = Math.ceil(Math.log2(numTransactions));
    }

    if (depth === leafDepth) {
        return hashes[0];
    }

    var maxLeavesPerChild = Math.pow(2, leafDepth - depth - 1);
    var prefix = depth === leafDepth - 1 ? leafPrefix : internalNodePrefix;
    if (numTransactions <= maxLeavesPerChild) {
        var left = calculateMerkleRoot(hashes, depth + 1, leafDepth);
        return hashConcat([prefix, left, nonExistingNodeHash]);
    }

    var left = calculateMerkleRoot(hashes.slice(0, maxLeavesPerChild), depth + 1, leafDepth);
    var right = calculateMerkleRoot(hashes.slice(maxLeavesPerChild), depth + 1, leafDepth);
    return hashConcat([prefix, left, prefix, right]);
}

function internalMerklePath(hashes, targetIndex, depth, leafDepth) {
    var numTransactions = hashes.length;

    if (depth === leafDepth) {
        return [];
    }

    var maxLeavesPerChild = Math.pow(2, leafDepth - depth - 1);
    var prefix = depth == leafDepth - 1 ? leafPrefix : internalNodePrefix;
    if (numTransactions <= maxLeavesPerChild) {
        var path = internalMerklePath(hashes, targetIndex, depth + 1, leafDepth);
        path.push({side: 1, hash: nonExistingNodeHash});
        return path;
    }

    if (targetIndex < maxLeavesPerChild) {
        var path = internalMerklePath(hashes.slice(0, maxLeavesPerChild), targetIndex, depth + 1, leafDepth);
        var right = calculateMerkleRoot(hashes.slice(maxLeavesPerChild), depth + 1, leafDepth);
        path.push({side: 1, hash: right});
    } else {
        var left = calculateMerkleRoot(hashes.slice(0, maxLeavesPerChild), depth + 1, leafDepth);
        var path = internalMerklePath(hashes.slice(maxLeavesPerChild), targetIndex - maxLeavesPerChild, depth + 1, leafDepth);
        path.push({side: 0, hash: left});
    }
    return path;
}

/*
 * a path looks like this:
 * {merklePath: [{side: <0|1>, hash: <hash buffer depth n-1>},
 *               {side: <0|1>, hash: <hash buffer depth n-2>},
 *               ...
 *               {side: <0|1>, hash: <hash buffer depth 1>}]}
 */
function merklePath(hashes, target) {
    if (!hashes || hashes.length == 0) {
        throw new Error("Cannot make merkle path from empty transaction set");
    }
    var index = -1;
    for (var i = 0; i < hashes.length; i++) {
        if (hashes[i].equals(target)) {
            index = i;
            break;
        }
    }
    if (index === -1) {
        throw new Error("Target is not in list of hashes");
    }

    var leafDepth = Math.ceil(Math.log2(hashes.length));
    var path = internalMerklePath(hashes, index, 0, leafDepth);
    return path;
}

/**
 *
 * @param path The merkle path to validate.
 * Format [{side: <0|1>, hash: <hash buffer depth n-1>},
 *         {side: <0|1>, hash: <hash buffer depth n-2>},
 *         ...,
 *         {side: <0|1>, hash: <hash buffer depth 1>}]

 * @param target the leaf hash that the path proves belongs in the merkleRoot
 * @param merkleRoot The merkle root that supposedly contains the target via the supplied path.
 * The merkle root is typically taken from a block header.
 */
function validateMerklePath(path, target, merkleRoot) {
    let currentHash = target;

    for (let i = 0; i < path.length; i++) {
        const item = path[i];

        const prefix = (i == 0) ? Buffer.from([1]) : Buffer.from([0]);

        if (item.side === 0) {
            currentHash = hashConcat([prefix, item.hash, prefix, currentHash]);
        } else {
            if (item.hash.equals(nonExistingNodeHash)) {
                currentHash = hashConcat([prefix, currentHash, nonExistingNodeHash]);
            } else {
                currentHash = hashConcat([prefix, currentHash, prefix, item.hash]);
            }
        }

    }

    return merkleRoot.equals(currentHash);
}


/**
 * @param content the content that the signature signs. It will be digested before validating.
 * @param pubKey The pubKey to validate the signature with
 * @param signature the signature to validate
 *
 * @return true if signature ok, false otherwise
 */
function checkSignature(content, pubKey, signature) {
    let digest = hash256(content)
    return checkDigestSignature(digest, pubKey, signature)
}

/**
 * @param digest the signed digest. It will not be digested before validating.
 * @param pubKey The pubKey to validate the signature with
 * @param signature the signature to validate
 *
 * @return true if signature ok, false otherwise
 */
function checkDigestSignature(digest, pubKey, signature) {
    return secp256k1.verify(digest, signature, pubKey);
}

/**
 * @param content to sign. It will be digested before signing.
 * @param privKey The private key to sign the content with
 *
 * @return the signature
 */
function sign(content, privKey) {
    if (!privKey) {
        throw new Error("Programmer error, missing privKey");
    }
    if (privKey.length != 32) {
        throw new Error(`Programmer error. Invalid key length. Expected 32, but got ${privKey.length}`);
    }
    var digestBuffer = sha256(content);
    console.log("Bytes to digest:" + content.toString('hex'))
    console.log("Digest to sign:" + digestBuffer.toString('hex'))
    console.log("privKey: " + privKey.toString('hex'));
    return signDigest(digestBuffer, privKey);
}

/**
 * @param digestBuffer to sign. It will not be digested before signing.
 * @param privKey The private key to sign the digest with
 *
 * @return the signature
 */
function signDigest(digestBuffer, privKey) {
    return secp256k1.sign(digestBuffer, privKey).signature;
}


/**
 * Creates a key pair (which usually represents one user)
 * @returns {{pubKey: Buffer, privKey: Buffer}}
 */
function makeKeyPair() {
    let privKey;
    do {
        privKey = crypto.randomBytes(32)
    } while (!secp256k1.privateKeyVerify(privKey))
    const pubKey = secp256k1.publicKeyCreate(privKey)
    return {pubKey, privKey}
}

/**
 * Generates a 16bytes TUID (Text unique ID) (a 32characters long string)
 * @returns string
 */
function makeTuid() {
    return crypto.randomBytes(16).toString('hex')
}

/**
 * Converts hex string to Buffer
 * @param key: string
 * @returns {Buffer}
 */
function toBuffer(key) {
    return Buffer.from(key, "hex");
}


/**
 * Verify that keypair is correct. Providing the private key, this function returns its associated public key
 * @param privKey: Buffer
 * @returns {{pubKey: Buffer, privKey: Buffer}}
 */
function verifyKeyPair(privKey) {
    const pubKey = secp256k1.publicKeyCreate(privKey);
    return {pubKey, privKey};
}