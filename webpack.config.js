var path = require('path');

module.exports = {
  entry: './index.js',
  output: {
    filename: 'postchain-client.js',
    libraryTarget: "umd",
    library: "PostchainClient"
  }
};

